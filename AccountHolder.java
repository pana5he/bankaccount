
/**
 * This class will hold information about the accountHolder
 *
 * @author Panashe Chatambudza  
 * @version 1.0
 */
public class AccountHolder
{
    // instance variables 
    private String firstname;
    private String lastname;
    

    /**
     * Constructor for objects of class AccountHolder
     */
    public AccountHolder(String firstnameIn, String lastnameIn)
    {
        // initialise instance variables
        firstname = firstnameIn;
        lastname = lastnameIn;
    }

    /**
     */
    public void setFirstname(String firstnameIn)
    {
        // put your code here
        firstname = firstnameIn;
    }
    
    public void setLastname(String lastnameIn)
    {
        lastname = lastnameIn;
    }
    
    /**
     * This method is used to retun the Firstname of the account holder
     * @return account owners firstname
     */
    public String getFirstname()
    {
        return this.firstname;
    }
    
    /**
     * Getter method for returning the account owners lastname
     * @return Owners lastname
     */
    public String getLastname()
    {
        return this.lastname;
    }
}
