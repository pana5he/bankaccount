
/**
 * The parent class for the various bank accounts that are going to be represented in the program
 * It is abstract so it can not be instantiated.
 * @author Panashe Chatambudza  
 * @version 1.0
 */
public abstract class BankAccount
{
    // instance variables 
    protected AccountHolder customer;
    protected int accNo;
    protected double currentBalance;

    /**
     * Constructor for objects of class BankAccount
     */
    public BankAccount(AccountHolder customerIn, int accNoIn, double currentBalanceIn)
    {
        // initialise instance variables
        customer = customerIn;
        accNo = accNoIn; 
        currentBalance = currentBalanceIn;   
    }

    
    /**
     * @param customerIn An AccountHolder object that you want to set as the owner of the account
     */
    public void setCustomer(AccountHolder customerIn)
    {
        this.customer = customerIn;
    }
    
    /**
     * 
     * @param accNoIn The account number that you what to set for that account
     */
    public void setAccNo(int accNoIn)
    {
        this.accNo = accNoIn;
    }
    
    /**
     * @param currentBalanceIn The amount that you want to set the balance too
     */
    public void setCurrentBalance(double currentBalanceIn)
    {
        this.currentBalance = currentBalanceIn;
    }
    
    /**
     * This method is used for getting the customer object which is the owner of the account
     * @return customer object
     */
    public AccountHolder getCustomer()
    {
        return customer;
    }
    
    /**
     * This method is used for returning the account number of the account
     * @return account number;
     */
    public int getAccNo()
    {
        return accNo;
    }
    
    /**
     * This method is used for returning the current balance of the account
     * @return The current balance of the account
     */
    public double getCurrentBalance()
    {
        return currentBalance;
    }
    
    /**
     * This method is used for withdrawing money form the account
     * @param amount The amount that you want to withdraw 
     */
    public void withdraw(double amount)
    {
        currentBalance -= amount;
        System.out.println("Result: Your new  balance is: " + this.getCurrentBalance());
        System.out.println("---------------------------------------------------------------------------------------"); //styling
    }
    
    /**
     * This method is used for deposting money into the account
     * @param amount The amount that you want to deposit into the account
     */
    public void deposit(double amount)
    {
        //To prevent entering negative amount
        if (amount > 0)
        {
            currentBalance += amount; 
            System.out.println("Result Your new balance is: " + this.getCurrentBalance());
            System.out.println("---------------------------------------------------------------------------------------"); //styling
        }
        else
        {
            System.out.println("Transaction Failed!");
            System.out.println("Result: Make sure you enter the correct amount");
            System.out.println("---------------------------------------------------------------------------------------"); //styling
        }

    }
    
    /**
     * This method is used for tranferring money from one account to the other
     * @param amount The amount that you want to tranfer
     * @param account A BankAccount instance that you want to transfer the money to.
     */
    public void transfer(double amount,BankAccount account)
    {
        currentBalance -= amount;
        account.setCurrentBalance(account.getCurrentBalance() + amount);
        System.out.println("Transaction completed!");
        System.out.println("Result: Your new balance is: " + this.getCurrentBalance());
        System.out.println("---------------------------------------------------------------------------------------"); //styling
    }
}
