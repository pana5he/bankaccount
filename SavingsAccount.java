
/**
 * Write a description of class SavingsAccount here.
 *
 * @author Panashe Chatambudza
 * @version 1.0
 */
public class SavingsAccount extends BankAccount
{
    
    private double interest;
    
    /**
     * Constructor for objects of class SavingsAccount
     */
    public SavingsAccount(AccountHolder customer, int AccNo, double currentBalance)
    {
        // initialise instance variables
        super(customer, AccNo, currentBalance);
    }

    /**
     * This method overrides the parent class method to add the ability to prevent withdrawing into a negative balance
     */
    public void withdraw(double amount)
    {
        // put your code here
        if(currentBalance - amount < 0)
        {
            System.out.println("Transaction Failed!");
            System.out.println("Result: Your do not have that amount of funds in our account!");
            System.out.println("---------------------------------------------------------------------------------------"); //styling
        }
        else 
        {
            currentBalance -= amount;
            System.out.println("Result: Your new balance is: " + this.getCurrentBalance());
            System.out.println("---------------------------------------------------------------------------------------"); //styling
        }
    }
    
    /**
     * Method for adding interest to the balance in the Savings Account at the end of the month.
     * 
     * @param Interest rate given as a decimal percentage
     */
    public void addInterest(double interestRate)
    {

        interest =  interestRate * currentBalance;
        this.currentBalance += interest;
    }
    
    /**
     * Overrides a method in the parent class to add the ability to prevent tranferring more money than is available in the savings account
     * @param amount The amount that is to be tranfered
     * @param account The BankAccount instance that you want to tranfer the amount to.
     */
    
    public void transfer(double amount, BankAccount account)
    {
        if(currentBalance - amount < 0)
        {
            System.out.println("Result: You don't have the balance to support that transfer");
            System.out.println("---------------------------------------------------------------------------------------"); //styling
        }
        else 
        {
            currentBalance -= amount;
            this.transfer(amount, account);
            System.out.println("Transaction completed!");
            System.out.println("Result: Sent " + amount + " " + account);
            System.out.println("---------------------------------------------------------------------------------------"); //styling
        }
    }
}
